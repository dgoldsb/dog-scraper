from dataclasses import dataclass
from typing import List


@dataclass
class Dog:
    name: str
    image_url: str
    traits: List[str]
