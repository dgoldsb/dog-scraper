from distributor import distribute_dogs
from filterer import filter_dogs
from scraper import scrape_all_dogs


dogs = scrape_all_dogs()
filtered_dogs = filter_dogs(dogs)
distribute_dogs(filtered_dogs)
