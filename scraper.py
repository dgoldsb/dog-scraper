from typing import Iterable

import requests
from bs4 import BeautifulSoup
from googletrans import Translator

from data_model import Dog

URL = 'https://www.doamsterdam.nl/dog/im-looking-for-a-dog/'


def scrape_all_dogs() -> Iterable[Dog]:
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    results = soup.find_all("div", class_="c-pet")

    for result in results:
        traits = []
        for trait in result.find_all("li", class_="list-inline__item pet__feature"):
            traits.append(Translator().translate(
                text=trait.text, dest="en", src="nl"
            ).text)
        dog = Dog(
            result.find("h4", "pet__name").text,
            result.find("img").attrs["src"],
            traits,
        )

        yield dog
