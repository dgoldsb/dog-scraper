from typing import Iterable

from data_model import Dog


def distribute_dogs(dogs: Iterable[Dog]):
    for dog in dogs:
        print(dog)
