from typing import Iterable

from data_model import Dog


def filter_dogs(dogs: Iterable[Dog]) -> Iterable[Dog]:
    for dog in dogs:
        # Start selecting your dog here! Write ``yield dog`` if you decide that a dog
        # should pass this filter, do nothing otherwise.
        yield dog
